/*
 * FRAM: Fast Reboot for Arnet's Modems
 * Copyright (C) 2015  Schmidt Cristian Hernán <schcriher@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "telnet.h"

#include <iostream>
#include <boost/bind.hpp>
#include <boost/regex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/algorithm/string.hpp>

using std::cout;
using std::equal;
using std::fixed;
using std::size_t;
using std::string;
using boost::bind;
using boost::regex;
using boost::system::error_code;
using boost::algorithm::trim_copy;
using boost::asio::ip::tcp;
using boost::asio::deadline_timer;
using boost::asio::error::would_block;

typedef boost::chrono::duration<double> second_type;


TelNet::TelNet(boost::posix_time::time_duration globaltimeout)
    : is_cr(false),
      is_lf(true),
      len_prev_write(0),
      socket(io_service),
      deadline(io_service),
      timeout(globaltimeout)
{
    cout << "FRAM Fast Reboot for Arnet's Modems\n";
    deadline.expires_at(boost::posix_time::pos_infin);
    check_deadline();
}

TelNet::~TelNet() {
    error_code ignored_ec;
    socket.close(ignored_ec);
    deadline.cancel();
    cout << "Source: github.com/schcriher/fram\n";
}

void TelNet::check_deadline() {
    if (deadline.expires_at() <= deadline_timer::traits_type::now()) {
        error_code ignored_ec;
        socket.close(ignored_ec);
        deadline.expires_at(boost::posix_time::pos_infin);
    }
    deadline.async_wait(bind(&TelNet::check_deadline, this));
}

void TelNet::read_until(const string& expected) {
    read_until(expected, expected.length());
}

template<typename T>
void TelNet::read_until(const T& expected, size_t expected_length) {
    error_code ec = would_block;
    size_t length;
    boost::asio::streambuf buffer;
    boost::asio::async_read_until(socket, buffer, expected,
            bind(&TelNet::pipe_rw, _1, _2, &ec, &length));
    do io_service.run_one(); while (ec == would_block);
    if (ec) {
        throw boost::system::system_error(ec);
    }
    std::istream is(&buffer);
    char c;
    size_t n;
    string s;
    while (is.get(c)) {
        switch (c) {
        case IAC:
            iac_response += c;      // Interpret As Command
            is.get(c);
            iac_response += ++c;    // WILL → WON'T, DO → DON'T
            is.get(c);
            iac_response += c;      // option
            break;

        case '\r':
            is_cr = true;
            set_newline(s);
            break;

        case '\n':
            is_lf = true;
            set_newline(s);
            break;

        default:
            s += c;
            break;
        }
    }
    n = s.length();
    if (len_prev_write < n) {
        int len = n - (len_prev_write + expected_length);
        if (len > 0) {
            if (s[len_prev_write + len - 1] == '\n')
                len -= 1;
            cache += s.substr(len_prev_write, len);
        }
    }
}

void TelNet::write(const string& petition) {
    error_code ec = would_block;
    size_t length;
    string data = iac_response + petition + newline();
    boost::asio::async_write(socket, boost::asio::buffer(data),
            bind(&TelNet::pipe_rw, _1, _2, &ec, &length));
    do io_service.run_one(); while (ec == would_block);
    if (ec) {
        throw boost::system::system_error(ec);
    }
    if (data.length() != length) {
        throw std::runtime_error("Incomplete request");
    }
    //'newline' returns '\n', '\r' or '\r\n', but in cache is always '\n'
    // so add 1 to 'petition.length' for 'len_prev_write'
    len_prev_write = petition.length() + 1;
    iac_response.clear();
}

void TelNet::connect(const string& host, const string& service) {
    deadline.expires_from_now(timeout);
    tcp::resolver::query query(host, service);
    tcp::resolver::iterator endpoint_iter = tcp::resolver(io_service).resolve(query);
    tcp::resolver::iterator iter;
    error_code ec = would_block;
    boost::asio::async_connect(socket, endpoint_iter,
            bind(&TelNet::pipe_c, _1, _2, &ec, &iter));
    do io_service.run_one(); while (ec == would_block);
    if (ec || !socket.is_open()) {
        throw boost::system::system_error(ec ? ec : boost::asio::error::operation_aborted);
    }
    cout << "Connected to " << iter->endpoint() << "\n";
}

void TelNet::login(const string& username, const string& password) {
    read_until("Login: ");
    cout << cache << '\n';
    cache.clear();
    write(username);
    read_until("Password: ");
    write(password);
    len_prev_write = 0;
    read_until(boost::regex("( > |Login: )"), 0);
    if (cache.find("Login incorrect") != string::npos) {
        throw std::runtime_error("Login incorrect");
    }
}

void TelNet::default_gateways_down() {
    gateways = get_default_gateways();
    if (gateways.empty()) {
        throw std::runtime_error("Modem not connected: reboot");
    }
    print_gateways(gateways, "old");
    cpu_timer.start();
    set_default_gateways("down");
    while (!get_default_gateways().empty()) {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(250));
    }
}

void TelNet::default_gateways_up() {
    gateways_type gw;
    set_default_gateways("up");
    while (!key_compare(gateways, gw)) {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(250));
        gw = get_default_gateways();
    }
    cpu_timer.stop();
    print_gateways(gw, "new");
    second_type elapsed = boost::chrono::nanoseconds(cpu_timer.elapsed().wall);
    cout.precision(3);
    cout << "Full reconnected in " << fixed << elapsed.count() << " seconds\n";
}

void TelNet::set_default_gateways(const string& action) {
    for (gateways_type::iterator it = gateways.begin(); it != gateways.end(); ++it) {
        write("ppp config " + it->first + " " + action);
        read_until(" > ");
    }
}

const boost::regex wan_show_regex{
    "[\\w.]+\\s+\\d+\\s+[\\w.]+\\s+(?<gateway>\\w+)\\s+\\w+\\s+\\w+\\s+(?<status>\\w+)\\s+(?<ip>[()\\w.]+)\\s+.+"
};

gateways_type TelNet::get_default_gateways() {
    cache.clear();
    write("wan show");
    read_until(" > ");
    cache += '\n';
    size_t pos=0, offset=0;
    boost::smatch match;
    gateways_type gw;
    while ((pos = cache.find('\n', offset)) != string::npos) {
        if (boost::regex_match(cache.substr(offset, pos - offset), match, wan_show_regex)) {
            if (match["status"] == "Connected") {
                gw[match["gateway"]] = match["ip"];
            }
        }
        offset = pos + 1;
    }
    return gw;
}

string TelNet::newline() {
    string s;
    if (is_cr)
        s += '\r';
    if (is_lf)
        s += '\n';
    return s;
}

void TelNet::pipe_c(
        const error_code& ec, const tcp::resolver::iterator& iter,
        error_code* out_ec, tcp::resolver::iterator* out_iter) {
    *out_ec = ec;
    *out_iter = iter;
}

void TelNet::pipe_rw(
        const error_code& ec, size_t length,
        error_code* out_ec, size_t* out_length) {
    *out_ec = ec;
    *out_length = length;
}

template <typename Map>
bool TelNet::key_compare(Map const &lhs, Map const &rhs) {
    // http://stackoverflow.com/a/8473603/2430102
    auto pred = [] (decltype(*lhs.begin()) a, decltype(a) b) {return a.first == b.first;};
    return lhs.size() == rhs.size() && equal(lhs.begin(), lhs.end(), rhs.begin(), pred);
}

void TelNet::print_gateways(gateways_type& gateways, const string& ip_status) {
    for (gateways_type::iterator it = gateways.begin(); it != gateways.end(); ++it)
        cout << "Gateway " << it->first << ": " << ip_status << " ip " << it->second << '\n';
}

void TelNet::set_newline(string& str) {
    int len = str.length();
    if (len > 0 && str[len-1] != '\n')
        str += '\n';
}
