/*
 * FRAM: Fast Reboot for Arnet's Modems
 * Copyright (C) 2015  Schmidt Cristian Hernán <schcriher@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __FRAM_TELNET_H__
#define __FRAM_TELNET_H__

#include <map>
#include <string>
#include <boost/asio.hpp>
#include <boost/timer/timer.hpp>

typedef std::map<std::string, std::string> gateways_type;


class TelNet {
public:
    enum : char {
        IAC = '\xFF', // interpret as command (255)
    };

    TelNet(boost::posix_time::time_duration globaltimeout);
    ~TelNet();

    void connect(const std::string& host, const std::string& service);
    void login(const std::string& username, const std::string& password);
    void default_gateways_down();
    void default_gateways_up();

private:
    bool is_cr;
    bool is_lf;
    std::string cache;
    std::string iac_response;
    std::size_t len_prev_write;						// include the '\n'
    gateways_type gateways;
    boost::timer::cpu_timer cpu_timer;
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket;
    boost::asio::deadline_timer deadline;
    boost::posix_time::time_duration timeout;		// global

    std::string newline();
    void check_deadline();
    gateways_type get_default_gateways();
    void set_default_gateways(const std::string& action);

    template<typename T>
    void read_until(const T& expected, size_t expected_length);
    void read_until(const std::string& expected);
    void write(const std::string& petition);

    static void pipe_c(
            const boost::system::error_code& ec, const boost::asio::ip::tcp::resolver::iterator& iter,
            boost::system::error_code* out_ec, boost::asio::ip::tcp::resolver::iterator* out_iter);
    static void pipe_rw(
            const boost::system::error_code& ec, std::size_t length,
            boost::system::error_code* out_ec, std::size_t* out_length);

    template <typename Map>
    static bool key_compare(Map const &lhs, Map const &rhs);

    static void print_gateways(gateways_type& gateways, const std::string& ip_status);
    static void set_newline(std::string& str);

    TelNet();										// default constructor not allowed
    TelNet(const TelNet &tn);						// copying constructor not allowed
    TelNet& operator= (const TelNet &tn);			// assignment not implemented
};

#endif//__FRAM_TELNET_H__
