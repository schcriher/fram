/*
 * FRAM: Fast Reboot for Arnet's Modems
 * Copyright (C) 2015  Schmidt Cristian Hernán <schcriher@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#if !defined(__cplusplus) || __cplusplus < 201103L
#error "A C++11 compiler is required!"
#endif

#include <iostream>
#include <exception>
#include <boost/asio.hpp>

#include "telnet.h"

int main(int argc, char *argv[]) {
    using std::cerr;
    using std::endl;
    using std::exception;
    using boost::asio::ip::tcp;

    if (argc != 3) {
        cerr << "usage " << argv[0] << " modem_host admin_password" << endl;
        return 1;
    }
    try {
        TelNet telnet(boost::posix_time::seconds(60));
        telnet.connect(argv[1], "telnet");
        telnet.login("admin", argv[2]);
        telnet.default_gateways_down();
        telnet.default_gateways_up();
    }
    catch (const exception& e) {
        cerr << e.what() << endl;
        return 1;
    }
    return 0;
}
