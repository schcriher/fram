CC=g++
CFLAGS=-c -W -Wall -Wconversion -Wshadow -Wcast-qual -Wwrite-strings -pedantic -O2 -std=c++11
LDFLAGS=-lpthread -lboost_system -lboost_thread -lboost_regex -lboost_timer
SOURCES=main.cpp telnet.cpp
EXECUTABLE=fram
OBJECTS=$(SOURCES:.cpp=.o)

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJECTS) $(EXECUTABLE)

